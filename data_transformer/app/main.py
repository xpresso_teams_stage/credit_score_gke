"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler, MinMaxScaler, OneHotEncoder

__author__ = "### Author ###"

logger = XprLogger("data_transformer",level=logging.INFO)


class DataTransformer(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="DataTransformer")
        """ Initialize all the required constansts and data her """
        self.train_file = "/data/train.csv"
        self.test_file = "/data/test.csv"

        self.train_out_file = "/data/train_transformed.csv"
        self.test_out_file = "/data/test_transformed.csv"

        #Normalizing Numeric columns
        self.scaler = StandardScaler()
        self.encoder = OneHotEncoder(sparse=False)

        self.num_features = ['age', 'bmi', 'fnlwgt', 'education-num',
                        'hours-per-week']
        self.cat_features = ['workclass', 'education', 'marital-status',
                        'occupation', 'relationship', 'race', 'sex',
                        'smoke', 'active', 'native-country']

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===


            train_data = pd.read_csv(self.train_file)
            test_data = pd.read_csv(self.test_file)

            # Dropping label before Data Transformation
            y_train = train_data['label']
            train_data.drop('label', axis=1, inplace=True)
            y_test = test_data['label']
            test_data.drop('label', axis=1, inplace=True)

            logger.info("Training Dataset")
            logger.info(train_data.head())
            logger.info("Testing Dataset")
            logger.info(test_data.head())

            #Normalization of numerical features

            self.scaler.fit(train_data[self.num_features].values)
            train_data[self.num_features] = self.scaler.transform(
                train_data[self.num_features].values)

            test_data[self.num_features] = self.scaler.transform(
                test_data[self.num_features].values)

            logger.info("Training Dataset Post Normalization of Numeric "
                        "Columns:")
            logger.info(train_data.head())
            logger.info("Testing Dataset Post Normalization of Numeric "
                        "Columns:")
            logger.info(test_data.head())


            # One-Hot Encoding Categorical Features
            self.encoder.fit(train_data[self.cat_features].values)
            train_data = self.one_hot_encode(train_data)
            test_data = self.one_hot_encode(test_data)

            logger.info("Training Dataset Post One hot encoding of "
                        "Categorical Columns:")
            logger.info(train_data.head())
            logger.info("Testing Dataset Post One hot encoding of  "
                        "Categorical Columns:")
            logger.info(test_data.head())

            train_data = pd.concat([train_data, y_train], axis=1)
            test_data = pd.concat([test_data, y_test], axis=1)

            train_data.to_csv(self.train_out_file, index=False)
            test_data.to_csv(self.test_out_file, index=False)

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()


    def one_hot_encode(self,X):
        df_cat = pd.DataFrame(self.encoder.transform(X[self.cat_features].values),
                              columns=self.encoder.get_feature_names()).astype(int)
        X.drop(self.cat_features, axis=1, inplace=True)
        X.reset_index(inplace=True, drop=True)
        df_cat.reset_index(inplace=True, drop=True)
        X = pd.concat([X, df_cat], axis=1)
        return X

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = DataTransformer()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
