cloudpickle==1.4.1
pandas
numpy
python-logstash-async
jsonmerge
scikit-learn==0.22.1
scipy==1.4.1
