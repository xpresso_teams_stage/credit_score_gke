__author__ = 'xpresso.ai'

import os
import sys
import argparse

if os.path.exists('packs.zip'):
    sys.path.insert(0, 'packs.zip')

try:
    import pyspark
except:
    import findspark
    findspark.init()
    import pyspark

from pyspark.sql import SparkSession
from pyspark.ml import Pipeline
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.evaluation import BinaryClassificationEvaluator
from pyspark.sql.types import IntegerType
from pyspark.ml.classification import RandomForestClassificationModel

from string_indexer.app.indexer import CustomStringIndexer
from string_indexer.app.indexer import LabelIndexer
from one_hot_encoder.app.encoder import CustomerOneHotEncoderEstimator
from vector_assembler.app.assembler import CustomVectorAssembler
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component \
    import XprPipeline


GS_BASE_PATH = "gs://dataprc-ust-poc/data/stroke_predictor"
GS_INPUT_TRAIN_DATA_LOCATION = GS_BASE_PATH + "/train.csv"
GS_INPUT_TEST_DATA_LOCATION = GS_BASE_PATH + "/test.csv"
GS_OUTPUT_MODEL_LOCATION = GS_BASE_PATH + "/output/models/model-"
GS_OUTPUT_PREDICTIONS_LOCATION = GS_BASE_PATH + "/output/predictions/predictions-"


class MyPipeline(XprPipeline):

    def __init__(self, sys_args=[]):
        XprPipeline.__init__(self, sys_args)

    @staticmethod
    def main(args):
        """
        Accepts the command line args and start the pipeline

        Arguments:
            args {sys.argv} -- all command line args
        
        Returns:
            [MyPipeline] -- object of current class `MyPipeline`
            [DataFrame] -- the data frame object of the input data read
        """
        sys_args = args[1:]
        pipeline = MyPipeline(sys_args=sys_args)
        pipeline_stages = pipeline.prepare_stages()
        pipeline.setStages(pipeline_stages)
        return pipeline, pipeline.start()

    def read_data(self, path, format, inferSchema, header, mode):
        data_df = self.spark_session.read.load(path, \
            format=format, \
            inferSchema=inferSchema, \
            header=header, \
            mode=mode)
        return data_df

    def cast_col_int(self, dataframe, col_name):
        df = dataframe.withColumn(col_name, dataframe[col_name].cast(IntegerType()))
        df.printSchema()
        return df

    def start(self):
        """ 
        Starts the pipeline
        Returns:
            pyspark.sql.DataFrame -- pyspark dataframe read source path
        """

        path = GS_INPUT_TEST_DATA_LOCATION
        data_df = self.read_data(path, "csv", "true", "true", "DROPMALFORMED")
        data_df.printSchema()
        pm = self.fit(data_df)
        data_df = pm.transform(data_df)
        data_df.printSchema()
        print(f'Returning data_df...', flush=True)
        return data_df
    
    def prepare_stages(self):

        """This can be used prepare the stages of the pipeline
        
        Returns:
            list -- list of XprComponents; and will be stages in the [Xpr|My]Pipeline

        """

        categoricalColumns = ['gender', 'age','hypertension',
                              'heart_disease','ever_married','work_type',
                              'Residence_type','smoking_status']

        numericColumns = ['avg_glucose_level','bmi']

        pipeline_stages = []

        for categoricalCol in categoricalColumns:
            stringIndexer = CustomStringIndexer(categoricalCol+'-indexer', self.xpresso_run_name, inputCol = categoricalCol, outputCol = categoricalCol + 'Index')
            encoder = CustomerOneHotEncoderEstimator(categoricalCol+'-encoder', self.xpresso_run_name, inputCols=[stringIndexer.getOutputCol()], outputCols=[categoricalCol + "classVec"])
            pipeline_stages += [stringIndexer, encoder]

        label_stringIdx = LabelIndexer('labelindexer', self.xpresso_run_name, inputCol = 'stroke', outputCol = 'label')
        pipeline_stages += [label_stringIdx]

        assemblerInputs = [c + "classVec" for c in categoricalColumns] + numericColumns
        assembler = CustomVectorAssembler('assembler', self.xpresso_run_name, inputCols=assemblerInputs, outputCol="features")
        assembler.setHandleInvalid("skip")
        pipeline_stages += [assembler]
        # pipeline stages design complete
        return pipeline_stages

    def stop(self):
        try:
            print('Stopping pipeline!')
            self.pipeline_completed()
            print('Stopping spark session!')
            self.spark_session.stop()
            print('All done!')
        except Exception as e:
            print(str(e))


if __name__ == "__main__":

    pipeline, df = MyPipeline.main(sys.argv)
    pipeline.name = 'model_evaluation'
    df.printSchema()
    
    model_path = f'{GS_OUTPUT_MODEL_LOCATION}{pipeline.xpresso_run_name}'
    rf_model = RandomForestClassificationModel.load(model_path)

    predictions = rf_model.transform(df)
    predictions.printSchema()

    path = f'{GS_OUTPUT_PREDICTIONS_LOCATION}{pipeline.xpresso_run_name}'
    predicts = predictions.select(['id', 'label', 'prediction'])
    print(type(predictions))
    print(type(predicts))
    predicts.printSchema()
    pipeline.save_predictions(predicts, path)

    area = 0
    loss = 1
    status = None
    try:
        evaluator = BinaryClassificationEvaluator(rawPredictionCol="rawPrediction")
        area = evaluator.evaluate(predictions, {evaluator.metricName: "areaUnderPR"})
        loss = 1 - area
        status = f'areaUnderPR: {area}'
        print(f'areaUnderPR status {status}')
    except Exception as e:
        msg = f'Exception when evaluating model! {str(e)}'
        print(msg)
        status = str(msg)
    
    print(f"areaUnderPR: {area}")

    report_status = {
            "status": {
                "status": status
            },
            "metric":{
                "areaUnderPR": area,
            }
        }

    pipeline.report_status(report_status)
    pipeline.stop()
